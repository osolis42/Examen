const espress = require('express');
const User = require('../models/user');
const async = require('async');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

function index(request, response, next) {

};

function login(request, response, next) {
  const email = request.body.email;
  const password = request.body.password;

  async.parallel({
    user: (callback) => {
      User.findOne({
        email: email
      }).exec(callback);
    }
  }, (err, results) => {
    const user = results.user;
    if (user) {
      bcrypt.hash(password, user.salt, function(err, hash) { // Genera hash del password para comparar con la base de datos
        if (hash === user.password) {
          const payload = { // Info que sirve para referenciar el usuario
            id: user._id
          }
          let token = jwt.sign(payload, '9671bf34abf8f17bf2b9a131267c9bf', {
            expiresIn: 86400
          });
          response.json({
            error: false,
            message:'Autentication correct',
            objs: {
              token: token
            }
          });
        } else {
          response.json({
            error: true,
            message:'Autentication incorrect',
            objs: {}
          });
        }
      });
    } else {
      response.json({
        error: true,
        message: 'Autentication incorrect',
        objs: {}
      });
    }
  });
};

module.exports = {
  index,
  login
}
