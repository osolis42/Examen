const espress = require('express');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const async = require('async');

function index(request, response, next) {
  const page = request.params.page ? request.params.page : 1;

  User.paginate({}, {page: page, limit: 10}, (err, objs)=>{
    if (err) {
      response.json({
        error: true,
        message: 'Cannot read the users :(',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message:'User list',
        objs: objs
      });
    }
  });
}

function create(request, response, next) {
  const name = request.body.name;
  const lastname = request.body.lastname;
  const email = request.body.email;
  const password = request.body.password;

  let user = new User();
  user.name = name;
  user.lastname = lastname;
  user.email = email;

  const saltRounds = 10;

  bcrypt.genSalt(saltRounds, function(err, salt) { // Encriptar contraseña
    bcrypt.hash(password, salt, function(err, hash) {

      user.password = hash;
      user.salt = salt;

      user.save((err, obj)=>{ // Método de crear usuario
        if (err) {
          response.json({
            error: true,
            message: 'Cannot save new user :(',
            objs: {}
          });
        } else {
          obj.password = null;
          obj.salt = null;

          response.json({
            error: false,
            message:'User saved :)',
            objs: obj
          });
        }
      });
    });
  });



  response.send("In /users/ -> Post")
}

function update(request, response, next) {
  response.send("In /users/ -> Put")
}

function remove(request, response, next) {
  const id = request.params.id;
  if (id) {
    User.remove({
      _id:id
    }, function(err) {
      if (err) {
        response.json({
          error: true,
          message:'Cannot delete user :(',
          objs: {}
        });
      } else {
        response.json({
          error: false,
          message:'User deleted :)',
          objs: {}
        });
      }
    });
  } else {
    response.json({
      error: true,
      message:'User not found :/',
      objs: {}
    });
  }
}

module.exports = {
  index,
  create,
  update,
  remove
}
