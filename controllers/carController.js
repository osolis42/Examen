const espress = require('express');
const Car = require('../models/car');

function index(request, response, next) {
  const page = request.params.page ? request.params.page : 1;

  Car.paginate({}, {page: page, limit: 10}, (err, objs)=>{
    if (err) {
      response.json({
        error: true,
        message: 'No se pudieron extraer los carros',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message:'Lista de Carros',
        objs: objs
      });
    }
  });
}

function getCarByModel(request, response, next) {
  const model = request.body.model;

  Car.findOne({ model: model }, (err, obj) => {
     if (err) {
       response.json({
         error: true,
         message: 'No se pudo extraer el carro',
         objs: {}
       });
     } else {
       response.json({
         error: false,
         message:'Carro',
         objs: objs
       });
     }
  });
}

function create(request, response, next) {
  const model = request.body.model;
  const description = request.body.description;
  const status = request.body.status;
  const color = request.body.color;

  let car = new Car();
  car.model = model;
  car.description = description;
  car.status = status;
  car.color = color;

  car.save((err, obj)=>{
    if (err) {
      response.json({
        error: true,
        message: 'Pelicula no  Guardada',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message:'Pelicula guardada',
        objs: obj
      });
    }
  });

  response.send("Estaś en /cars/ -> Post")
}

function update(request, response, next) {
  response.send("Estaś en /cars/ -> Put")
}

function remove(request, response, next) {
  const id = request.params.id;
  if (id) {
    Car.remove({
      _id:id
    }, function(err) {
      if (err) {
        response.json({
          error: true,
          message:'Pelicula no eliminada',
          objs: {}
        });
      } else {
        response.json({
          error: false,
          message:'Pelicula eliminada',
          objs: {}
        });
      }
    });
  } else {
    response.json({
      error: true,
      message:'No existe la Pelicula',
      objs: {}
    });
  }
}

module.exports = {
  index,
  getCarByModel,
  create,
  update,
  remove
}
