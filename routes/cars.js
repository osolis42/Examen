const express = require('express');
const router = express.Router();
const carController = require('../controllers/carController');

router.get('/:page?', carController.index);

router.get('/:id', carController.getCarByModel);

router.post('/', carController.create);

router.put('/:id', carController.update);

router.delete('/:id?', carController.remove);

module.exports = router;
